const imageContainer = document.querySelector('#imgs')
const leftBtn = document.querySelector('#left')
const rightBtn = document.querySelector('#right')
const imgs = document.querySelectorAll('#imgs img')

const INTERVAL = 3000

let idx = 0

document.body.style.backgroundImage = `url('${imageContainer.children[idx].src}')`

const run = () => {
    idx++
    changeImage()
}

let interval = setInterval(run, INTERVAL)

const changeImage = () => {
    if (idx > imgs.length - 1) {
        idx = 0
    } else if (idx < 0) {
        idx = imgs.length - 1
    }

    imageContainer.style.transform = `translateX(${-idx * 500}px)`
    document.body.style.backgroundImage = `url('${imageContainer.children[idx].src}')`
}

const resetInterval = () => {
    clearInterval(interval)
    interval = setInterval(run, INTERVAL)
}

rightBtn.addEventListener("click", () => {
    idx++
    changeImage()
    resetInterval()
})

leftBtn.addEventListener("click", () => {
    idx--
    changeImage()
    resetInterval()
})